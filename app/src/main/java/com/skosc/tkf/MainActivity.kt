package com.skosc.tkf

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.util.Log
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val LOGGER_TAG = "TKF_LOOPER_RX"

    private val thread = object : Thread() {
        lateinit var looper: Looper

        override fun run() {
            Looper.prepare()
            looper = Looper.myLooper()!!
            Looper.loop()
        }
    }

    private var counter = 1

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        thread.start()
        run_button.setOnClickListener {
            runRxChain()
        }

        Glide.with(this)
                .load(R.drawable.giphy)
                .into(funny_cat_pic)

    }

    private fun runRxChain() {
        val dataStream = Single.create {
            Log.i(LOGGER_TAG, "OBS: ${Thread.currentThread().name}: $counter")
            counter++
        }.observeOn(Looper.getMainLooper())
                .subscribeOn(thread.looper)

        dataStream.map {
            Log.i(LOGGER_TAG, "MAP1: ${Thread.currentThread().name}: $it")
            it * 2
        }.subscribe { cnt ->
            Log.i(LOGGER_TAG, "SUB1: ${Thread.currentThread().name}: $cnt")
            subscriber_text.text = "SUB1: ${Thread.currentThread().name}: $cnt"
        }

        dataStream.subscribe { cnt ->
            Log.i(LOGGER_TAG, "SUB2: ${Thread.currentThread().name}: $cnt")
            subscriber_text2.text = "SUB2: ${Thread.currentThread().name}: $cnt"
        }
    }
}
