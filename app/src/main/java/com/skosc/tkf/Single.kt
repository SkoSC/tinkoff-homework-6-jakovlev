@file:Suppress("DataClassPrivateConstructor")

package com.skosc.tkf

import android.os.Handler
import android.os.Looper

data class Single<T> private constructor(private val subLooper: Looper = Looper.getMainLooper(),
                                         private val obsLooper: Looper = subLooper,
                                         private val supplier: () -> T,
                                         private var resolvedValue: T? = null) {
    companion object {
        fun <T> create(supplier: () -> T): Single<T> {
            return Single(Looper.myLooper()!!, Looper.myLooper()!!, supplier, null)
        }
    }

    fun <V> map(transformer: (T) -> V): Single<V> {
        return Single(subLooper, obsLooper, {
            resolvedValue = supplier()
            transformer(resolvedValue!!)
        })
    }

    fun subscribeOn(looper: Looper): Single<T> {
        return copy(subLooper = looper)
    }

    fun observeOn(looper: Looper): Single<T> {
        return copy(obsLooper = looper)
    }

    fun subscribe(acceptor: (T) -> Unit) {
        Handler(subLooper, Handler.Callback {
            resolvedValue = if (resolvedValue == null) supplier() else resolvedValue
            Handler(obsLooper, Handler.Callback {
                acceptor(resolvedValue!!)
                true
            }).sendEmptyMessage(0)
            true
        }).sendEmptyMessage(0)
    }
}